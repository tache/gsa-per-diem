//
//  RateFetcher.h
//  GSA Per Diem
//
//  Created by Christopher Graham on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RateFetcher : NSObject{
  NSString *ratePlist;
  NSArray *rateContent;
//  NSMutableDictionary *rateSections;
}

@property (nonatomic, readonly) NSString *ratePlist;
@property (nonatomic, readonly) NSArray *rateContent;
//@property (nonatomic, readonly) NSMutableDictionary *rateSections;

- (id)initWithRateName:(NSString *)rateName;
- (NSDictionary *)rateItemAtIndex:(int)index;
- (int)rateCount;

@end
