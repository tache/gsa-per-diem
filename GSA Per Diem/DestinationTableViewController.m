//
//  DestinationTableViewController.m
//  GSA Per Diem
//
//  Created by Christopher Graham on 2/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DestinationTableViewController.h"
#import "DestinationDetailViewController.h"

@interface DestinationTableViewController ()

@end

@implementation DestinationTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
  self = [super initWithStyle:style];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewWillAppear:(BOOL)animated {
  dao = [[RateFetcher alloc] initWithRateName:@"gsa-per-diem-rates-201202"];
  [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

- (void)viewDidLoad
{
  [super viewDidLoad];
}

- (void)viewDidUnload
{
  [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [dao rateCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *CellIdentifier = @"Rate Cell";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  
  cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@",
                         [[dao rateItemAtIndex:indexPath.row] valueForKey:@"state"],
                         [[dao rateItemAtIndex:indexPath.row] valueForKey:@"destination"]];
  cell.detailTextLabel.text = [NSString stringWithFormat:@"(%@/%@) (%@ - %@)", 
                               [[dao rateItemAtIndex:indexPath.row] valueForKey:@"rate_lodging"], 
                               [[dao rateItemAtIndex:indexPath.row] valueForKey:@"rate_mie"],
                               [[dao rateItemAtIndex:indexPath.row] valueForKey:@"date_begin"], 
                               [[dao rateItemAtIndex:indexPath.row] valueForKey:@"date_end"]];
  
  return cell;
}

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
//  return [NSArray arrayWithObjects:@"A", @"C", @"D", @"F", @"G", @"I", @"L", @"M", @"N",  @"O", @"R",  @"T", @"V", @"W", nil ];
//}
//
//- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
//  return index;
//}

#pragma mark - Interface Seque

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  NSLog(@"DestinationTableViewController - prepareForSegue");
  
  if ([segue.identifier isEqualToString:@"Show Destination Detail"])	{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    //    Objective *objective = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    DestinationDetailViewController *destinationDetailViewController = segue.destinationViewController; 
//    NSLog(@"DestinationTableViewController -  [%@], ", indexPath);
    NSLog(@"DestinationTableViewController -  [%@], ",[dao rateItemAtIndex:indexPath.row]);
    
    [destinationDetailViewController resignFirstResponder];
    
    destinationDetailViewController.stateLabel.text = [[dao rateItemAtIndex:indexPath.row] valueForKey:@"state"];
    destinationDetailViewController.countyLabel.text = [[dao rateItemAtIndex:indexPath.row] valueForKey:@"county"];
    destinationDetailViewController.destinationLabel.text = [[dao rateItemAtIndex:indexPath.row] valueForKey:@"destination"];
    destinationDetailViewController.dateBeginLabel.text = [[dao rateItemAtIndex:indexPath.row] valueForKey:@"date_begin"];
    destinationDetailViewController.dateEndLabel.text = [[dao rateItemAtIndex:indexPath.row] valueForKey:@"date_end"];
    destinationDetailViewController.rateLodgingLabel.text = [NSString stringWithFormat:@"%@", [[dao rateItemAtIndex:indexPath.row] valueForKey:@"rate_lodging"]];
    destinationDetailViewController.rateMIELabel.text = [NSString stringWithFormat:@"%@", [[dao rateItemAtIndex:indexPath.row] valueForKey:@"rate_mie"]];
    
  }
}


@end
